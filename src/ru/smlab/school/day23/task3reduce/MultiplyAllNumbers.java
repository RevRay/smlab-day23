package ru.smlab.school.day23.task3reduce;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class MultiplyAllNumbers {
    /**
     * Посчитать результат перемножения всех чисел между собой
     * @param args
     */
    public static void main(String[] args) {
        List<Integer> list = List.of(5, 7, 8, 1, 5, 5, 4);

        int i = list.stream()
                .reduce( (accumulator, element) -> accumulator * element).get();
        System.out.println(i);
                //accumulator = 5
                //element = 7
        // accumulator 35
        // element = 8
        //accumulator 280
        // element = 1


        int[] arr = {1,2,3,10,9};

        List<String> list1 =  Arrays.stream(arr).mapToObj(j -> new String(j + "")).collect(Collectors.toList());
        System.out.println(list1);

    }
}
