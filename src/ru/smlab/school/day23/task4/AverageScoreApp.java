package ru.smlab.school.day23.task4;

import java.util.List;

public class AverageScoreApp {
    /**
     * Задание в классе 23.0.3
     * Даны списки баллов по каждому ученику.
     * Задача - посчитать средний бал для каждого, из средних баллов вывести наивысший.
     * @param args
     */
    public static void main(String[] args) {
        List<List<Integer>> scores = List.of(
                List.of(3, 2, 2, 4, 3, 3, 3),
                List.of(3, 2, 3, 4, 4, 4, 5),
                List.of(5, 4, 5, 5, 5, 5, 3),
                List.of(2, 2),
                List.of(5, 5, 5, 5, 5, 5, 4)
        );

//        scores.stream()
//                .mapToDouble(l -> l.size())


    }
}
