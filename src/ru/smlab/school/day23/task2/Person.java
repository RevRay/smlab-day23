package ru.smlab.school.day23.task2;

import java.util.Objects;

public class Person {
    String fullName;
    boolean gender;
    int age;

    public Person(String fullName, boolean gender, int age) {
        this.fullName = fullName;
        this.gender = gender;
        this.age = age;
    }

    @Override
    public String toString() {
        return String.format(
                "Сотрудник по имени '%s', пол: '%s', возраст: '%d'",
                fullName,
                gender ? "мужской" : "женский",
                age
        );
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Person person = (Person) o;
        return gender == person.gender && age == person.age && Objects.equals(fullName, person.fullName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(fullName, gender, age);
    }
}
