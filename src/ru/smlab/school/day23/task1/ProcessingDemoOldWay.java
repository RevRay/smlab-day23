package ru.smlab.school.day23.task1;

import java.util.*;

public class ProcessingDemoOldWay {
    /**
     * Задание в классе 23.0.1
     *
     * Дан список строк.
     * 1 Необходимо каждую строку превратить в число,
     * 2 отфильтровать только четные числа,
     * 3 оставшиеся числа отсортировать в порядке возрастания,
     * 4 из отсортированных значений оставить только первые три,
     * 5 после - вывести каждое на экран.
     *
     * (доплонение для стримов) 6* вместо пункта 5 - получить объект списка.
     * @param args
     */
    public static void main(String[] args) {
        List<String> stringNumbers = new ArrayList<>(
                Arrays.asList("23", "48", "12", "9", "106", "-10", "3", "7", "14", "3", "88")
        );

        //???

        List<Integer> numbers = new ArrayList<>();
        for (String s : stringNumbers) {
            numbers.add(Integer.valueOf(s));
        }

        Iterator<Integer> iter = numbers.iterator();
        while(iter.hasNext()) {
            if (iter.next() % 2 != 0) {
                iter.remove();
            }
        }

        Collections.sort(numbers);
        numbers = numbers.subList(0, 3);

        for(Integer i : numbers){
            System.out.println(i);
        }
    }
}
