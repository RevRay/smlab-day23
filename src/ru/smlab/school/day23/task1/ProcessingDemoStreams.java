package ru.smlab.school.day23.task1;

import java.util.*;
import java.util.function.Predicate;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class ProcessingDemoStreams {
    /**
     * Задание в классе 23.0.1
     *
     * Дан список строк.
     * 1 Необходимо каждую строку превратить в число,
     * 2 отфильтровать только четные числа,
     * 3 оставшиеся числа отсортировать в порядке возрастания,
     * 4 из отсортированных значений оставить только первые три,
     * 5 после - вывести каждое на экран.
     *
     * (доплонение для стримов) 6* вместо пункта 5 - получить объект списка.
     * @param args
     */
    public static void main(String[] args) {
        List<String> stringNumbers = new ArrayList<>(
                Arrays.asList("23", "48", "12", "9", "106", "-10", "3", "7", "14", "3", "88")
        );
        String[] strs = {"23", "48", "12", "9", "106", "-10", "3", "7", "14", "3", "88"};
//        List<Integer> results = stringNumbers.stream()
        List<Integer> nums = stringNumbers.stream()
                .map(s -> Integer.parseInt(s))
                .filter(integer -> integer % 3 == 0)
                .peek(s -> System.out.println(s))
                .sorted()
                .limit(50)
                .collect(Collectors.toList());

//        System.out.println(nums);
//        stringNumbers.stream()
//                .filter(s -> s.startsWith("1"))
////                        .forEach(s -> System.out.println(s));
//                        .forEachOrdered(System.out::println);
//                .count();
        System.out.println(nums);
    }
}
