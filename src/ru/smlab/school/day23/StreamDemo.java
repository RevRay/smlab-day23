package ru.smlab.school.day23;

import java.util.List;
import java.util.function.Consumer;

public class StreamDemo {
    public static void main(String[] args) {
        //+8 "[ value ]"
        List<Integer> list = List.of(1, 455, 3, 23, 90, 8, -8);
        list.stream().forEach(value -> System.out.println(value));

        list.stream().map(i -> { return String.format("[ %d ]", i+8); }).forEach(s -> System.out.println(s));
    }
}
