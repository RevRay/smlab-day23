package ru.smlab.school.day23.task2;

public class EmploymentHistory {
    String position;
    boolean retired;
    int yearsOfWork;

    public EmploymentHistory(String position, boolean retired, int yearsOfWork) {
        this.position = position;
        this.retired = retired;
        this.yearsOfWork = yearsOfWork;
    }

    @Override
    public String toString() {
        return "EmploymentHistory{" +
                "position='" + position + '\'' +
                ", retired=" + retired +
                ", yearsOfWork=" + yearsOfWork +
                '}';
    }
}
