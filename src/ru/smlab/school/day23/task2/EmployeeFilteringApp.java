package ru.smlab.school.day23.task2;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class EmployeeFilteringApp {
    /**
     * Задание в классе 23.0.2
     *
     * В компании есть список всех когда-либо работавших сотрудников.
     * Задача - выбрать только тех сотрудников-мужчин, которые старше 25 лет,
     * на текущий момент продолжают работать в компании, и отработали здесь более 3 лет.
     * Получить список имен сотрудников. Объект списка вывести на экран.
     * @param args
     */
    public static void main(String[] args) {
        Map<Person, EmploymentHistory> allEmployees = new HashMap<>();
        allEmployees.put(
                new Person("Иванов Иван Иванович", true, 34),
                new EmploymentHistory("охранник", false, 20)
        );
        allEmployees.put(
                new Person("Марков Станислав Ильич", true, 19),
                new EmploymentHistory("ассистент", false, 2)
        );
        allEmployees.put(
                new Person("Краснова Елена Михайловна", false, 52),
                new EmploymentHistory("бухглатер", false, 25)
        );
        allEmployees.put(
                new Person("Максимов Дмитрий Владимирович", true, 30),
                new EmploymentHistory("охранник", false, 5)
        );
        allEmployees.put(
                new Person("Столяров Максим Евгеньевич", true, 25),
                new EmploymentHistory("техник", false, 4)
        );
        allEmployees.put(
                new Person("Столбин Игорь Васильевич", true, 27),
                new EmploymentHistory("техник", true, 6)
        );

//        System.out.println(allEmployees);

        List<String> resultNames = allEmployees.entrySet().stream()
                .filter(e -> e.getKey().gender)
                .filter(e -> e.getKey().age > 25)
                .filter(e -> !e.getValue().retired)
                .filter(e -> e.getValue().yearsOfWork >= 3)
                .map(e -> e.getKey().fullName)
                .collect(Collectors.toList());

        System.out.println(resultNames);
    }
}
